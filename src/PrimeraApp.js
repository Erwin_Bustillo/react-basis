// import React, { Fragment } from 'react';
import React from "react";
import PropTypes from "prop-types";
// FC
const PrimeraApp = ({ saludo, subtitle }) => {
  const obj = {
    name: "Erwin",
    age: 50,
  };
  return (
    <>
      <h1>{saludo}</h1>
      <p>{subtitle}</p>
      <pre>{JSON.stringify(obj,null,3)}</pre>
    </>
  );
};

PrimeraApp.propType = {
  saludo: PropTypes.string.isRequired,
};

PrimeraApp.defaultProps = {
  subtitle: "Soy un subtitulo",
};

export default PrimeraApp;
