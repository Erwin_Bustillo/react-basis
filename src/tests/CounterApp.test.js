import { shallow } from "enzyme";
import CounterApp from "../CounterApp";

describe("CounterApp", () => {
  let wrapper = shallow(<CounterApp />);

  beforeEach(() => {
    wrapper = shallow(<CounterApp />);
  });

  test("should render CounterApp component", () => {
    expect(wrapper).toMatchSnapshot();
  });
  test("should return default value", () => {
    const wrapper = shallow(<CounterApp value={100} />);
    const counter = wrapper.find("h2").text().trim();
    expect(Number.parseInt(counter)).toBe(100);
  });

  test("should increment counter", () => {
    wrapper.find("button").at(0).simulate("click");
    const counter = wrapper.find("h2").text().trim();
    expect(counter).toBe("11");
  });

  test("should decrement counter", () => {
    wrapper.find("button").at(2).simulate("click");
    const counter = wrapper.find("h2").text().trim();
    expect(counter).toBe("9");
  });

  test("should reset counter", () => {
    wrapper.find("button").at(0).simulate("click");
    wrapper.find("button").at(0).simulate("click");
    wrapper.find("button").at(1).simulate("click");
    const counter = wrapper.find("h2").text().trim();
    expect(counter).toBe("10");
  });
});
