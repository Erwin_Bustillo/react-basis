import { getImagen } from "../../base/11-async-await";

describe("Test async await fetch", () => {
  test("should return url of image ", async () => {
    const url = await getImagen();
    expect(url.includes("https://")).toBe(true);
  });
});
