import { getUser, getUsuarioActivo } from "../../base/05-funciones";

describe("Test on functions file", () => {
  test("should return an object from getUser", () => {
    const obj = {
      uid: "ABC123",
      username: "El_Papi1502",
    };
    const user = getUser();
    expect(user).toEqual(obj);
  });

  test("should return an object from getUsuarioActivo", () => {
    const obj = {
      uid: "ABC567",
      username: "ErwinALX",
    };

    const user = getUsuarioActivo("ErwinALX");
    expect(user).toEqual(obj);
  });
});
