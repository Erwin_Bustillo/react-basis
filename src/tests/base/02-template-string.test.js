import { getSaludo } from "../../base/02-template-string";

describe("Test on template strings", () => {
  test("should return hello from getSaludo", () => {
    const name = "Fernando";
    const greet = getSaludo(name);
    expect(greet).toBe("Hola " + name);
  });

  test("should return hello erwin is name not provided", () => {
    const greet = getSaludo();
    expect(greet).toBe("Hola Erwin");
  });
});
