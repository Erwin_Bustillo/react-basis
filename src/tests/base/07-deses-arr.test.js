import { retornaArreglo } from "../../base/07-deses-arr";

describe("Test on destructuring", () => {
  test("should return an array ", () => {
    // const arr = retornaArreglo();
    const [letras, numeros] = retornaArreglo();
    // expect(arr).toEqual(["ABC", 123]);
    expect(letras).toBe("ABC");
    expect(numeros).toBe(123);
  });
});
