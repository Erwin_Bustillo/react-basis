import { getHeroeById, getHeroesByOwner } from "../../base/08-imp-exp";
import heroes from "../../data/heroes";

describe("Tests on heroes functions", () => {
  test("should return a hero by id", () => {
    const id = 1;
    const hero = getHeroeById(id);
    const heroData = heroes.find((h) => h.id === id);
    expect(hero).toEqual(heroData);
  });

  test("should return undefined if hero not exists", () => {
    const id = 10;
    const hero = getHeroeById(id);
    expect(hero).toBe(undefined);
  });

  test("should return an array with Dc heroes", () => {
    const owner = "DC";
    const heroes = getHeroesByOwner(owner);

    const heroesData = heroes.filter((h) => h.owner === owner);

    expect(heroes).toEqual(heroesData);
  });

  test("should return marvel heroes", () => {
    const owner = "Marvel";
    const heroes = getHeroesByOwner(owner);

    const heroesData = heroes.filter((h) => h.owner === owner);

    expect(heroes.length).toBe(2);
  });
});
