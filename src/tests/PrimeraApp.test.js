import React from "react";
import PrimeraApp from "../PrimeraApp";
import { shallow } from "enzyme";
describe("Pruebas en <PrimeraApp />", () => {
  test("should show  PrimeraApp component", () => {
    const saludo = "Hola, Soy Goku";
    const wrapper = shallow(<PrimeraApp saludo={saludo} />);

    expect(wrapper).toMatchSnapshot();
  });

  test("should send subtitle by props", () => {
    const saludo = "Hola, Soy Goku";
    const subtitle = `I'm a subtitle`;
    const wrapper = shallow(<PrimeraApp saludo={saludo} subtitle={subtitle} />);

    const textParagraph = wrapper.find("p").text();
    expect(textParagraph).toBe(subtitle);
  });
});
